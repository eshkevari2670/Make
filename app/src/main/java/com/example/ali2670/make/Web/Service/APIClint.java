package com.example.ali2670.make.Web.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class APIClint {

    public static Retrofit retrofit = null;
    public static String baseurl = "http://88.99.46.166:81/api/Location/";

    public static Retrofit getclint()
    {

        Gson gson = new GsonBuilder ()
                .setLenient()
                .create();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(baseurl).addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return retrofit;
    }
}
