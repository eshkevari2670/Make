package com.example.ali2670.make.Web.Service;

import com.example.ali2670.make.Model.SendData;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface APIInterface {
    @POST("SaveData")
    Call<String>SEND_DATA_CALL(@Query ("IMEI") String IMEI
            , @Query ("date") String date
            , @Query ("latitude") double latitude
            , @Query ("longitude") double longitude
            , @Query ("altitude") int altitude
            , @Query ("angle") int angle
            , @Query ("satellites")int satellites
            , @Query ("speed")int speed);
}
