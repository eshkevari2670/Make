package com.example.ali2670.make;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btnlost, btnhelptravel, btnnvigtion, btnrequirment, btnhotl, btndoa, btnsimcard, btnsarafi, btnresturan;
    boolean gpsstatus=false;
    TextView txttool;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.main);
        final Animation tranlatanimationhid = AnimationUtils.loadAnimation (this,R.anim.translatehid);
        final Animation animationalpha = AnimationUtils.loadAnimation (this,R.anim.alpha);
        findview ( );
        checkgpsstatus ();
        if (gpsstatus=false) {
            gpsenable ( );
        }
    }

    public void findview() {
        Typeface typeface = Typeface.createFromAsset (getAssets (),"font/ns.ttf");
        btnlost = findViewById (R.id.Act1_btn_lost);
        btnlost.setTypeface (typeface);
        btnhelptravel = findViewById (R.id.Act1_btn_travelhelp);
        btnhelptravel.setTypeface (typeface);
        btnnvigtion = findViewById (R.id.Act1_btn_navigation);
        btnnvigtion.setTypeface (typeface);
        //btnhotl = (Button) findViewById (R.id.Act1_btn_hotel);
        btndoa = findViewById (R.id.Act1_btn_doa);
        btndoa.setTypeface (typeface);
       //btnsimcard = (Button) findViewById (R.id.Act1_btn_simcard);
        //btnsarafi = (Button) findViewById (R.id.Act1_btn_sarafi);
       // btnresturan = (Button) findViewById (R.id.Act1_btn_resturant);
       txttool = findViewById (R.id.act1_txt_tolbar);
       txttool.setTypeface (typeface);
       drawerLayout = findViewById (R.id.mainnav);
       navigationView = findViewById (R.id.nvi);
    }

    public void runactivity(View v) {

        Intent i = null;
        int id = v.getId ( );
        if (id == R.id.Act1_btn_lost){
            i = new Intent (MainActivity.this, lost_act2.class);
        }
        else if (id == R.id.Act1_btn_travelhelp)
            i = new Intent (MainActivity.this, travelhelp_act3.class);
        else if (id==R.id.Act1_btn_navigation)
            i=new Intent (MainActivity.this,MapsActivity_act4.class);
        else if ((id==R.id.Act1_btn_doa))
            i=new Intent (MainActivity.this,doa_act5.class);
        startActivity (i);
    }

    public void gpsenable(){
        Intent intent21 = new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity (intent21);
    }
    public void checkgpsstatus(){
        LocationManager locationManager = (LocationManager)getSystemService (Context.LOCATION_SERVICE);
        gpsstatus=locationManager.isProviderEnabled (LocationManager.GPS_PROVIDER);
    }
    public void onoptionmenoitemeselect(MenuItem item){
        int id =item.getItemId ();
        Intent i = null;
        if (id==R.id.Home);
        {
            i = new Intent (this, MainActivity.class);
            startActivity (i);
        }
        if (id==R.id.lostact)
        {
            i=new Intent (this,lost_act2.class);
            startActivity (i);
        }
    }
}
