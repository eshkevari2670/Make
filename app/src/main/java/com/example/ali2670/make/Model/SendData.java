package com.example.ali2670.make.Model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class SendData {
   @SerializedName("IMEI")
    int IMEI;
    @SerializedName("date")
    String date;
    @SerializedName("latitude")
     double latitude;
    @SerializedName("longitude")
    double longitude;
    @SerializedName("altitude")
    int altitude=0;
    @SerializedName("angle")
    int angle=0;
    @SerializedName("satellites")
    int satellites=96;
    @SerializedName("speed")
    int speed=0;

    public int getIMEI() {
        return IMEI;
    }

    public void setIMEI(int IMEI) {
        this.IMEI = IMEI;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getSatellites() {
        return satellites;
    }

    public void setSatellites(int satellites) {
        this.satellites = satellites;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String toString(){
        return
                "SendData{"+
                        "IMEI = '" + IMEI + '\'' +
                        ",date='"+date +'\'' +
                        ",latitude= '" + latitude + '\'' +
                        ",longitude = '" + longitude + '\'' +
                        ",altitude = '" + altitude + '\'' +
                        ",angle = '" + angle + '\'' +
                        ",satellites = '" + satellites + '\'' +
                        ",speed = '" + speed + '\'' +
                        "}";

    }
}
