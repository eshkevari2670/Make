package com.example.ali2670.make;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.AlteredCharSequence;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.ali2670.make.Model.SendData;
import com.example.ali2670.make.Web.Service.APIClint;
import com.example.ali2670.make.Web.Service.APIInterface;

import java.util.Date;
import java.util.Locale;

import pl.droidsonroids.gif.GifImageView;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.N)
public class lost_act2 extends AppCompatActivity implements LocationListener {
    public static final int requestpermissionCode = 1;
    EditText zaercod, sname, fname, nationalcod, hotel, manager, managermobile;
    double lattxt=0;
    double longtxt=0;
    GifImageView waitgif;
    Button send, getlocation;
    LocationManager locationManager;
    String date;

    //IMEI=359049082846696
    //latitude=35.749695795535
    //longitude=51.3281921469496
    int altitude=0;
    int angle=0;
    int satellites=96;
    int speed=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.act2_lost);
        findview ( );
        gpsoffon ();
        send.setVisibility (View.INVISIBLE);
        waitgif.setVisibility (View.VISIBLE);
        long dates = System.currentTimeMillis();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("MMM MM dd, yyyy h:mm a");
         date = sdf.format (dates);
         Toast.makeText (lost_act2.this,date,Toast.LENGTH_LONG).show ();
        EnableRuntimePermision ( );
        LocationManager locationManager = (LocationManager) getSystemService (Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission (this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates (LocationManager.GPS_PROVIDER, 0, 0, lost_act2.this);
        final Location location = locationManager.getLastKnownLocation (LocationManager.GPS_PROVIDER);
        if (location != null) {
            lattxt=location.getLatitude ( );
            longtxt= location.getLongitude ( );
        }
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String im =zaercod.getText ().toString ();
               if (im.isEmpty ()==false) {
                   postdata (im, date, lattxt, longtxt, altitude, angle, satellites, speed);
               }
               else {
                   Toast.makeText (lost_act2.this,"لطفا کد زائر وارد کنید",Toast.LENGTH_LONG).show ();
               }

            }
        });
    }


    @SuppressLint("WrongViewCast")
    public void findview() {
        zaercod = findViewById (R.id.Act2_txt_zaercod);
        sname = findViewById (R.id.act2_txt_Sname);
        fname = findViewById (R.id.act2_txt_Fname);
        nationalcod = findViewById (R.id.act2_txt_natinalcod);
        //hotel = (EditText) findViewById (R.id.act2_txt_hotel);
        //manager = (EditText) findViewById (R.id.act2_txt_managername);
        //managermobile = (EditText) findViewById (R.id.act2_txt_managermobile);
        //lattxt = (TextView) findViewById (R.id.Act2_txtview_lat);
        //longtxt = (TextView) findViewById (R.id.Act2_txtview_long);
        send = findViewById (R.id.act2_btn_send);
        waitgif= findViewById (R.id.act2_gif_wait);
        //getlocation = (Button) findViewById (R.id.act2_btn_getlocation);
    }

    @Override
    public void onLocationChanged(Location location) {
        lattxt=location.getLatitude ( );
        longtxt=location.getLongitude ( );
        if (lattxt !=0) {
            send.setVisibility (View.VISIBLE);
            waitgif.setVisibility (View.INVISIBLE);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public void EnableRuntimePermision() {
        if (ActivityCompat.shouldShowRequestPermissionRationale (lost_act2.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Toast.makeText (lost_act2.this, "ACCESS_FINE_LOCATION permission allows us to Access GPS in app", Toast.LENGTH_LONG).show ( );

        } else {
            ActivityCompat.requestPermissions (lost_act2.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestpermissionCode);
        }
    }
    public void postdata(String IMEI, String date, double latitude, double longitude, int altitude, int angle, int satellites, int speed){
        APIInterface apiInterface = APIClint.getclint().create(APIInterface.class);
        retrofit2.Call <String> call=apiInterface.SEND_DATA_CALL(IMEI,date,latitude,longitude,altitude,angle,satellites,speed);
        call.enqueue(new Callback <String>() {
            @Override
            public void onResponse(retrofit2.Call<String> call, Response<String> response) {
                if (response.isSuccessful()){

                       Toast.makeText (lost_act2.this,response.body (), Toast.LENGTH_LONG).show ( );

                }else {
                    Toast.makeText(lost_act2.this,response.body (),Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(retrofit2.Call<String> call, Throwable t) {
                Toast.makeText (lost_act2.this,t.getMessage (),Toast.LENGTH_LONG).show ();
                fname.setText (t.getMessage ());

            }
        });
    }
    public void gpsoffon(){
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            AlertDialog.Builder alertd = new AlertDialog.Builder (lost_act2.this);
            alertd.setTitle ("GPS");
            alertd.setMessage ("موقعیت مکانی شما غیر فعال است. آیا مایلید آن را فعال کنید؟");
            alertd.setNegativeButton ("no",null);
            alertd.setPositiveButton ("yes", new DialogInterface.OnClickListener ( ) {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(settingsIntent);
                }
            });
            alertd.show ();

        }

    }

}
